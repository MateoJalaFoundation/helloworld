﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            byte number = 10;
            int count = 1;
            float price = 10.95f;
            char character = 'A';
            string name = "Mateo";
            bool isWorking = true;

            Console.WriteLine(number);
            Console.WriteLine(count);
            Console.WriteLine(price);
            Console.WriteLine(character);
            Console.WriteLine(name);
            Console.WriteLine(isWorking);

            Console.WriteLine("{0} {1}", int.MinValue,int.MaxValue);
        }
    }
}
